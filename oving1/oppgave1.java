package Ovinger.oving1;

public class oppgave1 {
    public static void beregning(double tommer) {
        double rate = 2.54;
        double cm = tommer * rate;
        System.out.println(tommer + " tommer blir " + cm + " cm.");
    }
    public static void main(String[] args) {
        beregning(2);
        beregning(4);
        beregning(4.5);
    }
}

