package Ovinger.oving1;

public class oppgave2 {
    public static void beregning(int timer, int min, int sek) {
        int totSek = (timer*60*60) + (min*60) + sek;
        System.out.println(timer + " timer, " + min + " minutter og " + sek + " sekunder blir til " + totSek + " sekunder.");
    }
    public static void main(String[] args) {
        beregning(1, 2, 54);
        beregning(3, 56, 0);
        beregning(5, 29, 4);
    }
}
