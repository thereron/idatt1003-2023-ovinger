package Ovinger.oving1;

public class oppgave3 {
    public static void beregning(int sek) {
        int timer = sek/(60*60);
        int minutt = (sek - (timer*60*60))/60;
        int restSek = (sek - (timer*60*60) - (minutt*60));

        System.out.println(sek + " sek blir til " + timer + " timer, " + minutt + " min og " + restSek + " sekunder.");
    }
    public static void main(String[] args) {
       beregning(3660);
       beregning(4598);
       beregning(981347);
    }
}
