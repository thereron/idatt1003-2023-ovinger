package Ovinger.oving10.oppg1;

public class Arrangement {
  private final int arNr;
  private final String arNavn;
  private final String arSted;
  private final String arrangor;
  private final String arType;
  private final long arTidspunkt;
  private final int arDato;
  public Arrangement(int arNr, String arNavn, String arSted, String arrangor, String arType, long arTidspunkt) {
    this.arNr = arNr;
    this.arNavn = arNavn;
    this.arSted = arSted;
    this.arrangor = arrangor;
    this.arType = arType;
    this.arTidspunkt = arTidspunkt;
    this.arDato = (int) (arTidspunkt/10000);
  }

  public int getArNr() {
    return arNr;
  }

  public String getArNavn() {
    return arNavn;
  }

  public String getArSted() {
    return arSted;
  }

  public String getArrangor() {
    return arrangor;
  }

  public String getArType() {
    return arType;
  }

  public long getArTidspunkt() {
    return arTidspunkt;
  }

  public int getArDato() {
    return arDato;
  }

  @Override
  public String toString() {
    String tidspunkt = String.valueOf(arTidspunkt);
    String stringTid = tidspunkt.substring(6, 8) + "/" +  tidspunkt.substring(4,6) + "/" + tidspunkt.substring(0, 4) + ", klokka " + tidspunkt.substring(8);
    return "Arrangement: " + getArNavn() + ", Sted: " + getArSted() + ", Arrangør: " + getArrangor() + ", Type: " + getArType() + ", Tidspunkt: " + stringTid;
  }

  public static void main(String[] args) {
    Arrangement arrr = new Arrangement(1001, "h", "h", "h", "h", 200210301800L);
    System.out.println(arrr.toString());
    //System.out.println(arrr.getArDato());
  }
}
