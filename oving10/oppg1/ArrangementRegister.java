package Ovinger.oving10.oppg1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Comparator;

public class ArrangementRegister {
  private ArrayList<Arrangement> arrangementRegister;

  public ArrangementRegister() {
    this.arrangementRegister = new ArrayList<Arrangement>();
  }

  public ArrayList<Arrangement> getArrangementRegister() {
    return arrangementRegister;
  }

  public void registrerAr(int arNr, String arNavn, String arSted, String arrangor, String arType, long arTidspunkt) { //komposisjon - lagrer det rett inn i registeret
    Arrangement arrangement = new Arrangement(arNr, arNavn, arSted, arrangor, arType, arTidspunkt);
    arrangementRegister.add(arrangement);
  }


  public ArrayList<Arrangement> arListeGittSted(String sted) {
    ArrayList<Arrangement> arrSted = new ArrayList<Arrangement>();
    for (Arrangement arrangement : arrangementRegister) {
      if (Objects.equals(arrangement.getArSted(), sted)) {
        arrSted.add(arrangement);
      }
    }
    return arrSted; //ar for arrangement, arr for array
  }

  public ArrayList<Arrangement> arListeGittDato(int dato) {
    ArrayList<Arrangement> arrDato = new ArrayList<Arrangement>();
    for (Arrangement arrangement : arrangementRegister) {
      if (arrangement.getArDato() == dato) {
        arrDato.add(arrangement);
      }
    }
    return arrDato;
  }

  public ArrayList<Arrangement> arListeMellomDato(int dato1, int dato2) {
    ArrayList<Arrangement> arrMellomDato = new ArrayList<>();
    for (Arrangement arrangement : arrangementRegister) {
      if (arrangement.getArDato() > dato1 && arrangement.getArDato() < dato2) {
        arrMellomDato.add(arrangement);
      }
    }
    arrMellomDato.sort(new sortByTidspunkt());
    return arrMellomDato;
  }

  public String sortArSted() {
    ArrayList<Arrangement> arSortSted = new ArrayList<>(arrangementRegister);
    Comparator<Arrangement> sortBySted = (Arrangement a1, Arrangement a2) -> a1.getArSted().compareTo(a2.getArSted());

    arSortSted.sort(sortBySted);

    StringBuilder string = new StringBuilder();
    for (Arrangement ar : arSortSted) {
      string.append(ar.toString()).append("\n");
    }
    return string.toString();
  }

  public String sortArType() {
    ArrayList<Arrangement> arSortType= new ArrayList<>(arrangementRegister);
    Comparator<Arrangement> sortByType = (Arrangement a1, Arrangement a2) -> a1.getArType().compareTo(a2.getArType());

    arSortType.sort(sortByType);

    StringBuilder string = new StringBuilder();
    for (Arrangement ar : arSortType) {
      string.append(ar.toString()).append("\n");
    }
    return string.toString();
  }

  public String sortArTidspunkt() {
    ArrayList<Arrangement> arSortTidspunkt= new ArrayList<>(arrangementRegister);
    arSortTidspunkt.sort(new sortByTidspunkt());

    StringBuilder string = new StringBuilder();
    for (Arrangement ar : arSortTidspunkt) {
      string.append(ar.toString()).append("\n");
    }
    return string.toString();
  }

}

class sortByTidspunkt implements Comparator<Arrangement> { //fått hjelp fra ChatGPT
  private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");

  public int compare(Arrangement a1, Arrangement a2) {
    try {
      Date date1 = dateFormat.parse(Long.toString(a1.getArTidspunkt()));
      Date date2 = dateFormat.parse(Long.toString(a2.getArTidspunkt()));

      return date1.compareTo(date2);
    } catch (ParseException e) {
      return 0;
    }
  }
}

/*class sortBySted implements Comparator<Arrangement> {
  public int compare(Arrangement a1, Arrangement a2) {
    return a1.getArSted().compareTo(a2.getArSted());
  }
}*/

/*class sortByType implements Comparator<Arrangement> {
  public int compare(Arrangement a1, Arrangement a2) {
    return a1.getArType().compareTo(a2.getArType());
  }
}*/


