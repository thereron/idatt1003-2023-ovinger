package Ovinger.oving10.oppg1;

import java.util.ArrayList;
import java.util.Scanner;

public class oppgave1 {
  public static ArrangementRegister arrangementRegister;

  public static void init() {
    arrangementRegister = new ArrangementRegister();
    arrangementRegister.registrerAr(1001, "Svanesjøen", "Operaen", "Den trønderske ballett", "Ballet", 202402201830L);
    arrangementRegister.registrerAr(1002, "Postgirobygget", "Hallen", "Musikk og slikt co.", "Konsert", 202310301800L);
    arrangementRegister.registrerAr(1003, "Barnehagekoret", "Operaen", "Trondheim barnehage", "Konsert", 202310290900L);
    arrangementRegister.registrerAr(1004, "Nidarosdomen Guided Tour", "Nidarosdomen", "Trondheims turistkontor", "Guidet tur", 202310291900L);
  }

  public static void main(String[] args) {
    init();
    Scanner input = new Scanner(System.in);
    int valg;

    do {
      System.out.println("Velg en handling:");
      System.out.println("1. Registrere et nytt arrangement");
      System.out.println("2. Finn alle arrangementer på et gitt sted");
      System.out.println("3. Finn alle arrangementer på en gitt dato");
      System.out.println("4. Finn alle arrangementer innenfor et gitt tidsintervall");
      System.out.println("5. Lag liste over arrangementene sortert etter sted, type eller tid");
      System.out.println("6. Avslutt");

      System.out.print("Skriv inn valget ditt: ");
      valg = input.nextInt();
      input.nextLine();

      switch (valg) {
        case 1:
          System.out.print("Skriv inn arrangementets nummer (arNr): ");
          int arNr = input.nextInt();
          input.nextLine();

          System.out.print("Skriv inn arrangementets navn (arNavn): ");
          String arNavn = input.nextLine();

          System.out.print("Skriv inn arrangementets sted (arSted): ");
          String arSted = input.nextLine();

          System.out.print("Skriv inn arrangørens navn (arrangor): ");
          String arrangor = input.nextLine();

          System.out.print("Skriv inn arrangementets type (arType): ");
          String arType = input.nextLine();

          System.out.print("Skriv inn tidspunktet for arrangementet (arTidspunkt): ");
          long arTidspunkt = input.nextLong();
          input.nextLine();
          arrangementRegister.registrerAr(arNr, arNavn, arSted, arrangor, arType, arTidspunkt);
          break;
        case 2:
          System.out.println("Skriv inn sted: ");
          String sted = input.nextLine();
          ArrayList<Arrangement> arListeGittSted = arrangementRegister.arListeGittSted(sted);
          System.out.println(printListe(arListeGittSted));
          break;
        case 3:
          System.out.println("Skriv inn dato: ");
          int dato = input.nextInt();
          input.nextLine();
          ArrayList<Arrangement> arListeGittDato = arrangementRegister.arListeGittDato(dato);
          System.out.println(printListe(arListeGittDato));
          break;
        case 4:
          System.out.println("Skriv inn startdatoen: ");
          int startDato = input.nextInt();
          input.nextLine();

          System.out.println("Skriv inn sluttdatoen: ");
          int sluttDato = input.nextInt();
          input.nextLine();

          ArrayList<Arrangement> arListeMellomDato = arrangementRegister.arListeMellomDato(startDato, sluttDato);
          System.out.println(printListe(arListeMellomDato));
          break;
        case 5:
          System.out.println("Velg et av alternativene: ");
          System.out.println("1. Sorter etter sted");
          System.out.println("2. Sorter etter type");
          System.out.println("3. Sorter etter tidspunkt");
          int sortValg = input.nextInt();
          input.nextLine();
          if (sortValg == 1) {
            System.out.println(arrangementRegister.sortArSted());
          } else if (sortValg == 2) {
            System.out.println(arrangementRegister.sortArType());
          } else if (sortValg == 3) {
            System.out.println(arrangementRegister.sortArTidspunkt());
          } else {
            System.out.println("Du skrev et ugyldig tall.");
          }
          break;
        case 6:
          System.out.println("Programmet avsluttes.");
          break;
        default:
          System.out.println("Ugyldig valg. Prøv igjen.");
      }
    } while (valg != 6);

    input.close();
  }
  public static String printListe(ArrayList<Arrangement> liste) {
    StringBuilder string = new StringBuilder();
    for (Arrangement ar : liste) {
      string.append(ar.toString()).append("\n");
    }
    return string.toString();
  }
}
