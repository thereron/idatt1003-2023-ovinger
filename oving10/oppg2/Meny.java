package Ovinger.oving10.oppg2;
public class Meny {
  private final Rett forrett;
  private final Rett hovedrett;
  private final Rett dessert;

  public Meny(Rett forrett, Rett hovedrett, Rett dessert) {
    this.forrett = forrett;
    this.hovedrett = hovedrett;
    this.dessert = dessert;
  }

  public Rett getForrett() {
    return forrett;
  }

  public Rett getHovedrett() {
    return hovedrett;
  }

  public Rett getDessert() {
    return dessert;
  }

  public double menyTotPris() {
    return getForrett().getPris() + getHovedrett().getPris() + getDessert().getPris();
  }

  @Override
  public String toString() {
    return getForrett().toString() + getHovedrett().toString() + getDessert().toString();
  }
}
