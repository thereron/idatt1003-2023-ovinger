package Ovinger.oving10.oppg2;

import java.util.ArrayList;

public class MenyRegister {
  private ArrayList<Rett> retter;
  private ArrayList<Meny> menyer;
  public MenyRegister() {
    retter = new ArrayList<Rett>();
    menyer = new ArrayList<Meny>();
  }

  public ArrayList<Rett> getRetter() {
    return retter;
  }

  public ArrayList<Meny> getMenyer() {
    return menyer;
  }

  public void registrerRett(String navn, String type, Double pris, String oppskrift) {
    Rett nyRett = new Rett(navn, type, pris, oppskrift);
    retter.add(nyRett);
  }

  public Rett finnRettGittNavn(String navn) {
    for (Rett rett : getRetter()) {
      if (rett.getNavn().equalsIgnoreCase(navn)) {
        return rett;
      }
    }
    return null;
  }

  public ArrayList<Rett> finnRetterGittType(String type) {
    ArrayList<Rett> arrRetterType = new ArrayList<>();
    for (Rett rett : getRetter()) {
      if (rett.getType().equalsIgnoreCase(type)) {
        arrRetterType.add(rett);
      }
    }
    return arrRetterType;
  }

  public void registrerMeny(Rett forrett, Rett hovedrett, Rett dessert) {
    Meny meny = new Meny(forrett, hovedrett, dessert);
    menyer.add(meny);
  }

  public ArrayList<Meny> menyerInnenforIntervall(double startPris, double sluttpris) {
    ArrayList<Meny> arrMeny = new ArrayList<>();
    for (Meny meny : getMenyer()) {
      double totPris = meny.menyTotPris();
      if(totPris >= startPris && totPris <=sluttpris) {
        arrMeny.add(meny);
      }
    }
    return arrMeny;
  }

}
