package Ovinger.oving10.oppg2;

import java.util.ArrayList;

public class oppgave2 {
  public static void main(String[] args) { //ikke særlig pen testklient
    MenyRegister menyRegister = new MenyRegister();

    menyRegister.registrerRett("Spaghetti Carbonara", "Hovedrett", 189.90, "Kok spaghetti, stek bacon, bland med egg og parmesan, og server.");
    menyRegister.registrerRett("Kylling Alfredo", "Hovedrett", 199.90, "Stek kyllingbryst, kok fettuccine pasta, og lag en kremet Alfredo-saus.");
    menyRegister.registrerRett("Caesar Salat", "Hovedrett", 175.90, "Kutt salat, tilsett krutonger, parmesan, og Caesar-dressing.");
    menyRegister.registrerRett("Sjokoladekake", "Dessert", 89.90, "Bland mel, kakao, sukker, egg, smør, og bak i ovnen til den er ferdig.");
    menyRegister.registrerRett("Hvitløksbrød", "Forrett", 59.90, "Smør hvitløkssmør på en baguett og stek den.");
    menyRegister.registrerRett("Bruschetta", "Forrett", 89.90, "Kutt opp tomater og putt det oppå en baguette.");
    menyRegister.registrerRett("Ostekake", "Dessert", 105.90, "Lag den.");

    menyRegister.registrerMeny(menyRegister.finnRettGittNavn("Hvitløksbrød"), menyRegister.finnRettGittNavn("Spaghetti Carbonara"), menyRegister.finnRettGittNavn("Sjokoladekake"));
    menyRegister.registrerMeny(menyRegister.finnRettGittNavn("Bruschetta"), menyRegister.finnRettGittNavn("Kylling Alfredo"), menyRegister.finnRettGittNavn("Ostekake"));

    System.out.println("---Rett gitt navn: (Sjokoladekake)---");
    System.out.println(menyRegister.finnRettGittNavn("Sjokoladekake").toString());

    System.out.println("\n---Liste med retter gitt type: (forrett)---");
    System.out.println(menyRegister.finnRetterGittType("forrett").toString()); //print til terminal er ikke pen

    System.out.println("\n---Meny innenfor prisintervall:---");
    ArrayList<Meny> prisMenyer = menyRegister.menyerInnenforIntervall(200, 550);
    for (Meny meny : prisMenyer) {
      System.out.println("MENY: ");
      System.out.println(meny.toString());
    }
  }
}

