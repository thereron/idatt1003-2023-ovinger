import java.util.Scanner;

public class Brukergrensesnitt {
  private Eiendomsregister eiendomsregister;

  private final Scanner scan = new Scanner(System.in);

  private final int LEGG_TIL_EIENDOM = 1;
  private final int VIS_ALLE_EIENDOMMER = 2;
  private final int FINN_EIENDOM = 3;
  private final int BEREGN_GJENNOMSNITTSAREAL = 4;
  private final int FINN_EIENDOMMER_GITT_GNR = 5;
  private final int AVSLUTT = 6;

  public void init() {
    eiendomsregister = new Eiendomsregister();

    Eiendom eiendom1 = new Eiendom(1445, "Gloppen", 77, 631, 1017.6, "Jens Olsen");
    Eiendom eiendom2 = new Eiendom(1445, "Gloppen", 77, 131, "Syningom", 661.3, "Nicolay Madsen");
    Eiendom eiendom3 = new Eiendom(1445, "Gloppen", 75, 19, "Fugletun", 650.6, "Evilyn Jensen");
    Eiendom eiendom4 = new Eiendom(1445, "Gloppen", 74, 188, 1457.2, "Karl Ove Bråten");
    Eiendom eiendom5 = new Eiendom(1445, "Gloppen", 69, 47, "Høiberg", 1339.4, "Elsa Indregård");

    eiendomsregister.registrerEiendom(eiendom1);
    eiendomsregister.registrerEiendom(eiendom2);
    eiendomsregister.registrerEiendom(eiendom3);
    eiendomsregister.registrerEiendom(eiendom4);
    eiendomsregister.registrerEiendom(eiendom5);
  }

  public void start() {
    boolean finished = false;
    while (!finished) {
      int menyValg = this.visMeny();

      switch (menyValg) {
        case LEGG_TIL_EIENDOM:
          leggTilEiendom();
          break;
        case VIS_ALLE_EIENDOMMER:
          System.out.println(eiendomsregister.toString());
          break;
        case FINN_EIENDOM:
          finnEiendom();
          break;
        case BEREGN_GJENNOMSNITTSAREAL:
          System.out.println("Gjennomsnittsarealet er: " + eiendomsregister.getGjennomsnittAreal());
          break;
        case FINN_EIENDOMMER_GITT_GNR:
          printEiendommerGittGnr();
          break;
        case AVSLUTT:
          System.out.println("Thank you for using the Properties app!\n");
          finished = true;
          break;
        default:
          System.out.println("Unrecognized menu selected..");
          break;
      }
    }
  }

  private int visMeny() {
    System.out.println("\n***** MENY *****\n");
    System.out.println("1. Legg til eiendom");
    System.out.println("2. Skriv ut alle eiendommene");
    System.out.println("3. Søk etter eiendom");
    System.out.println("4. Beregn gjennomsnittsareal");
    System.out.println("5. Skriv ut alle eiendommer med gitt gårdsnummer");
    System.out.println("6. Avslutt");
    System.out.println("\nVennligst skriv inn et tall mellom 1 og 5.\n");
    int menyValg = 0;
    if (scan.hasNextInt()) {
      menyValg = scan.nextInt();
      scan.nextLine();
    } else {
      System.out.println("Du må skrive inn et tall, ikke tekst");
    }
    return menyValg;
  }

  private void leggTilEiendom() {
    System.out.println("***** Registrer eiendom *****");

    System.out.println("Kommunenummer: ");
    int kommuneNr = scan.nextInt();
    scan.nextLine();

    System.out.println("Kommunenavn: ");
    String kommunenavn = scan.nextLine();

    System.out.println("Gårdsnummer: ");
    int gnr = scan.nextInt();
    scan.nextLine();

    System.out.println("Bruksnummer: ");
    int bnr = scan.nextInt();
    scan.nextLine();

    System.out.println("Bruksnavn: (trykk enter hvis bruksnavn ikke eksisterer)");
    String bruksnavn = scan.nextLine();

    System.out.println("Areal i m2: ");
    double areal = scan.nextDouble();
    scan.nextLine();

    System.out.println("Navn på eier: ");
    String navn = scan.nextLine();

    try {
      Eiendom eiendom = new Eiendom(kommuneNr, kommunenavn, gnr, bnr, bruksnavn, areal, navn); //har prøvd å kode med aggregation i denne oppgaven,
      //men hadde det vært bedre med sånn composition? Hva er på en måte nytten av at jeg har kodet med aggregation her? Kan man lokaliseere
      //eiendommene dersom registeret blir sletta?
      eiendomsregister.registrerEiendom(eiendom);
      System.out.println("Eiendommen ble registrert!");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private void finnEiendom() {
    System.out.println("For å finne eiendommen må du oppgi kommunenr, gårdsnummer og bruksnummer: ");

    System.out.println("Kommunenummer: ");
    int kommuneNr = scan.nextInt();
    scan.nextLine();

    System.out.println("Gårdsnummer: ");
    int gnr = scan.nextInt();
    scan.nextLine();

    System.out.println("Bruksnummer: ");
    int bnr = scan.nextInt();
    scan.nextLine();

    try {
      Eiendom eiendom = eiendomsregister.getEiendom(kommuneNr, gnr, bnr);
      System.out.println(eiendom.toString());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private void printEiendommerGittGnr() {
    System.out.println("Oppgi gårdsnummeret: ");
    int gnr = scan.nextInt();
    scan.nextLine();
    //if registeret er tomt så gir man beskjed til bruker. Altså ikke bruke try catch her. Returner en verdi som gir deg beskjed om du ikke traff noe
    try {
      Eiendomsregister gnrRegister = eiendomsregister.getEiendomGittGnr(gnr);
      System.out.println(gnrRegister.toString());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
