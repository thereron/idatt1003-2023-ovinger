/** Klassen Eiendom.
 *
 * @author Therese S. Rondeel
 */
public class Eiendom {
  private final int kommuneNr;
  private final String kommuneNavn;
  private final int gnr;
  private final int bnr;
  private final String bruksnavn;
  private final double areal;
  private String eier; //eier kan endre seg

  /** Konstruktør som oppretter et objekt av typen Eiendom.
   *
   * @param kommuneNr kommunenummer
   * @param kommuneNavn navnet til kommunen
   * @param gnr gårdsnummer
   * @param bnr bruksnummer
   * @param bruksnavn bruksnavn
   * @param areal areal oppgitt i m2
   * @param eier navnet til eieren
   */
  public Eiendom(int kommuneNr, String kommuneNavn, int gnr, int bnr, String bruksnavn,
                 double areal, String eier) {
    if (kommuneNr < 101 || kommuneNr > 5054) {
      throw new IllegalArgumentException("Kommunenummeret må være fra og med 101 til og med 5054");
    }
    if (gnr < 1 || bnr < 1 || areal <= 0) {
      throw new IllegalArgumentException("Gårdsnummer, bruksnummer og areal må være positive "
          + "tall.");
    }
    this.kommuneNr = kommuneNr;
    this.kommuneNavn = kommuneNavn;
    this.gnr = gnr;
    this.bnr = bnr;
    this.bruksnavn = bruksnavn;
    this.areal = areal;
    this.eier = eier;
  }

  /** Konstruktør som oppretter et objekt av typen Eiendom, men uten bruksnavn.
   *
   * @param kommuneNr kommunenummer
   * @param kommuneNavn kommunenavn
   * @param gnr gårdsnummer
   * @param bnr bruksnummer
   * @param areal areal oppgitt i m2
   * @param eier eier
   */
  public Eiendom(int kommuneNr, String kommuneNavn, int gnr, int bnr, double areal, String eier) {
    if (kommuneNr < 101 || kommuneNr > 5054) {
      throw new IllegalArgumentException("Kommunenummeret må være fra og med 101 til og med 5054");
    }
    if (gnr < 1 || bnr < 1 || areal <= 0) {
      throw new IllegalArgumentException("Gårdsnummer, bruksnummer og areal må være positive "
          + "tall.");
    }
    this.kommuneNr = kommuneNr;
    this.kommuneNavn = kommuneNavn;
    this.gnr = gnr;
    this.bnr = bnr;
    this.bruksnavn = "";
    this.areal = areal;
    this.eier = eier;
  }

  public int getKommuneNr() {
    return kommuneNr;
  }

  public String getKommuneNavn() {
    return kommuneNavn;
  }

  public int getGnr() {
    return gnr;
  }

  public int getBnr() {
    return bnr;
  }

  public String getBruksnavn() {
    return bruksnavn;
  }

  public double getAreal() {
    return areal;
  }

  public String getEier() {
    return eier;
  }

  /**
   * Mutator-metode for objektvariabelen eier. Valgt å implementere denne metoden fordi en eier av
   * et gårdsbruk kan endre seg når eiendommen selges.
   *
   * @param eier navnet til den nye eieren
   */
  public void setEier(String eier) {
    this.eier = eier;
  }

  public String getId() {
    return getKommuneNr() + "-" + getGnr() + "/" + getBnr();
  }

  @Override
  public String toString() {
    if (getBruksnavn().isEmpty()) {
      return "Kommune: " + getKommuneNavn() + ", Kommunenr: " + getKommuneNr() + ", Gårdsnr: "
          + getGnr() + ", Bruksnr: " + getBnr() + ", Areal (m2): " + getAreal()  + ", eier: "
          + getEier();
    } else {
      return "Kommune: " + getKommuneNavn() + ", Kommunenr: " + getKommuneNr() + ", Gårdsnr: "
          + getGnr() + ", Bruksnr: " + getBnr() + ", Bruksnavn: " + getBruksnavn()
          + ", Areal (m2): " + getAreal()  + ", eier: " + getEier();
    }
  }
}
