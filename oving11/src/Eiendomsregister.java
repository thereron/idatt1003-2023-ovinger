import java.util.ArrayList;
import java.util.List;

/** Klassen Eiendomsregister.
 *
 * @author Therese S. Rondeel
 */
public class Eiendomsregister {

  /** Valgte å bruke ArrayList blant annet fordi vi ønsker et register som kan endre seg i
   * størrelse, slik at man kan både legge til og fjerne eiendommer, noe som er spesifisert at skal
   * være en funksjonalitet i oppgavebeskrivelsen. Fixed size array har ikke denne evnen.
   * Samtidig kunne HashMap være gunstig med tanke på å ha id til eiendom-objektet som key, og
   * eiendom-objekt som value. HashMap skal være raskere enn ArrayList når det kommer til å
   * finne ulike objekter.
   * I oppgaven skal vi finne eiendommer ved hjelp av id og gårdsnummer, og har kun en metode
   * hvor hver av de benyttes, og det blir dermed sett på som mest gunstig med Arraylist.
   */
  private ArrayList<Eiendom> eiendomsRegister;

  public Eiendomsregister() {
    this.eiendomsRegister = new ArrayList<>();
  }

  public Eiendomsregister(ArrayList<Eiendom> register) {
    this.eiendomsRegister = register;
  }

  public ArrayList<Eiendom> getEiendomsRegister() {
    return (ArrayList<Eiendom>) eiendomsRegister.clone();
  }

  public void registrerEiendom(Eiendom eiendom) {
    eiendomsRegister.add(eiendom);
  }

  public void slettEiendom(Eiendom eiendom) {
    eiendomsRegister.remove(eiendom);
  }

  public int antallEiendommer() {
    return getEiendomsRegister().size();
  }

  /** Metode for å finne eiendom ved bruk av kommunenr, gnr og bnr.
   *
   * @param kommuneNr kommunenummeret
   * @param gnr gårdsnummeret
   * @param bnr bruksnummeret
   * @return eiendommen som skal finnes. Finnes den ikke returneres null.
   */
  public Eiendom getEiendom(int kommuneNr, int gnr, int bnr) {
    String id = kommuneNr + "-" + gnr + "/" + bnr;
    for (Eiendom eiendom : getEiendomsRegister()) {
      if (id.equals(eiendom.getId())) {
        return eiendom;
      }
    }
    throw new RuntimeException("Det ble ikke funnet en eiendom med id: " + id);
  }

  /** Metode for å regne ut gjennomsnittsarealet ved hjelp av stream.
   *
   * @return gjennomsnittsarealet
   */
  public double getGjennomsnittAreal() {
    return getEiendomsRegister().stream()
        .mapToDouble(Eiendom::getAreal)
        .average()
        .orElse(0.0);
  }

  /** Metode for å finne alle eiendommer med et gitt gårdsnummer.
   *
   * @param gnr tar gårdsnummer som et parameter
   * @return returnerer et nytt Eiendomsregister slik at vi enkelt kan bruke toString på denne
   */
  public Eiendomsregister getEiendomGittGnr(int gnr) { //hvordan kan jeg gjøre denne bedre?
    List<Eiendom> listRegisterGittGnr = getEiendomsRegister().stream()
        .filter(eiendom -> gnr == eiendom.getGnr())
        .toList();
    if (listRegisterGittGnr.isEmpty()) { //bruker jeg throw på en riktig/god måte her?
      throw new RuntimeException("Det fantes ingen eiendommer med gårdsnummeret: " + gnr);
    } else {
      ArrayList<Eiendom> arrListRegisterGittGnr = new ArrayList<>(listRegisterGittGnr);
      return new Eiendomsregister(arrListRegisterGittGnr);
    }
  }

  @Override
  public String toString() {
    StringBuilder string = new StringBuilder();
    for (Eiendom eiendom : getEiendomsRegister()) {
      string.append(eiendom.toString()).append("\n");
    }
    return string.toString();
  }

}