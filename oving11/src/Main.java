public class Main {
  public static void main(String[] args) {
    Brukergrensesnitt brukergrensesnitt = new Brukergrensesnitt();
    brukergrensesnitt.init();
    brukergrensesnitt.start();
  }

 //komposisjon hadde vært best
  /** Hvordan koden opppfyller:
   * COHESION:
   * For å oppnå høy cohesion i denne oppgaven har jeg på klasse-nivå sørget for at registeret over
   * eiendommer, og eiendommene i seg selv, er hver sine klasser, slik at de da representerer hver
   * sin enhet.
   * På metode-nivå har jeg oppnådd høy cohesion ved å sørge for at hver metode har ansvar for kun en
   * bestemt oppgave, ved at f.eks. printEiendomGittGnr kun har i oppgave å printe ut beskjed til
   * bruker. Den benytter metoder i Eiendomsregister-klassen slik at den ikke får flere oppgaver,
   * og kun da har i oppgave å skrive ut til bruker.
   *
   * COUPLING:
   * Da vi gjerne ønsker loose coupling har jeg implementert dette ved å benytte aggregering, som
   * er mulig siden eiendommer kan eksistere uten et register. Siden eiendomsregisteret har
   * referanse til Eiendoms-objektene, vil endringer i Eiendoms-objektene automatisk vises i
   * Eiendomsregisteret.
   * Dermed får Eiendom- og Eiendomsregister-klassene hver sine klart definerte ansvarsområder.
   * Eiendom representerer de ulike eiendommene, mens Eiendomsregister håndterer registrering av
   * eiendommer, samt funksjoner som å se gjennomsnittsareal over alle eiendommer.
   *
   * Men jeg er ikke helt sikker på dette jeg har skrevet over, det jeg fortsatt ikke har full
   * forståelse for disse konseptene, så jeg vil gjerne vite mer om hvordan jeg har oppnådd disse
   * tingene, eventuelt hvilke ting jeg har gjort som motvirker disse konseptene, og som jeg dermed
   * burde endre på.
   */
}
