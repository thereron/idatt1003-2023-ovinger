package Ovinger.oving2;

import java.util.Scanner;

public class oppgave1 {
    public static void skuddaar() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Årstall?");
        int aar = scan.nextInt();
        if ((aar % 4 == 0 && aar % 100 != 0) || aar % 400 == 0) {
            System.out.println(aar + " er et skuddår!");
        } else {
            System.out.println(aar + " er ikke et skuddår:(");
        }
        scan.close();
    }
    public static void main(String[] args) {
        skuddaar();
    }
}
