package Ovinger.oving3;

import static javax.swing.JOptionPane.*;

public class oppgave1 {
    public static void gangetabell(int startTall, int sluttTall) {
        for (int aktueltTall = startTall; aktueltTall <= sluttTall; aktueltTall++) {
            System.out.println(aktueltTall + "-gangen:");
            for (int runde = 1; runde <= 10; runde ++) {
                int gangetTall = aktueltTall * runde;
                System.out.println(aktueltTall + " x " + runde + " = " + gangetTall);
            }
        }

    }
    public static void main(String[] args) {
        int tall1;
        int tall2;
        do{
            showMessageDialog(null, "Skriv inn tallene for hvilken del av gangetabelllen du ønsker å få skrevet ut");
            String inputTall1 = showInputDialog("Hvilket tall ønsker du å starte med?");
            String inputTall2 = showInputDialog("Hvilket tall ønsker du å slutte med?");
            tall1 = Integer.parseInt(inputTall1);
            tall2 = Integer.parseInt(inputTall2);
        } while (tall1 > tall2);

        gangetabell(tall1, tall2);
    }
}
