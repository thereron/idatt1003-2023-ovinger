package Ovinger.oving3;

import static javax.swing.JOptionPane.*;

public class oppgave2 {
    public static void primtall(int a) {
        int antallMuligeDelinger = 0;
        for (int i = a; i > 0; i--) {
            if (a % i == 0) {
                antallMuligeDelinger++;
            }
        } // denne for løkka vil bruke mye tid og strøm, da den går gjennom alle tallene i fra a til 0. For å gjør dette lettere for maskinen burde man f.eks. sjekke først om et tall er delelig på 2. Da trenger man kun å gå gjennom halvparten av disse tallene. Man kan også sjekke med f.eks. 3 og bli kvitt enda flere. Det er lurt å sjekke nettet for en  mer strøm og belastnings-effektiv løsning!
        if (antallMuligeDelinger == 2) {
            System.out.println(a + " er et primtall!");
        } else {
            System.out.println(a + " er ikke et primtall.");
        }
    }

    public static void main(String[] args) {
        boolean fortsettProgram = true;
        int tall;
        do {

            do {
                String inputTall = showInputDialog("Skriv inn tallet du ønsker å sjekke om er et primtall");
                tall = Integer.parseInt(inputTall);
            } while (tall < 1);
            
            primtall(tall);

            String inpfortsette = showInputDialog("Ønsker du å fortsette å sjekke om tall er primtall? Tast 1 for ja og 2 for nei.");
            int fortsette = Integer.parseInt(inpfortsette);
            if (fortsette == 2) {
                fortsettProgram = false;
            }
        } while (fortsettProgram);

    }
}
