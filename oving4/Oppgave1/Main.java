package Ovinger.oving4.Oppgave1;

import java.util.Scanner;

public class Main {
    public static Valuta[] valuta = new Valuta[3];
    
    public static void main(String[] args) {
        valuta[0] = new Valuta("dollar", 10.87f);
        valuta[1] = new Valuta("euro", 11.59f);
        valuta[2] = new Valuta("svenske kroner", 0.97f);

        regnOm();
    }

    public static void regnOm() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Velg valuta:");
        System.out.println("1 : dollar");
        System.out.println("2: euro");
        System.out.println("3: svenske koner");
        System.out.println("4 : avslutt");

        int inpV = scan.nextInt();
        if(inpV == 4) {
            System.out.println("Du har valgt å avslutte.");
        } else if (inpV == 1 || inpV == 2 || inpV == 3) {
            System.out.println("Skriv inn beløpet i " + valuta[inpV - 1].navn);
            float antall = scan.nextFloat();
            System.out.println(Valuta.utregning(antall, valuta[inpV - 1]));
            sjekkNyOmregning();
        } else {
            System.out.println("Husk at du skal skrive et tall mellom 1 og 4!");
            regnOm();
        }

    }
    public static void sjekkNyOmregning() {
        Scanner scan = new Scanner(System.in);
        int inpNyOm = 0;
        System.out.println("Skriv 1 hvis du ønsker å regne om et nytt beløp med en annen eller samme valuta.");
        inpNyOm = scan.nextInt();
        if(inpNyOm == 1) {
            regnOm();
        } else {
            System.out.println("Du har valgt å ikke regne om et nytt beløp");
        }
    }
}