package Ovinger.oving4.Oppgave1;

public class Valuta {
    String navn;
    float verdi;
    public Valuta(String navn, float verdi) {
        this.navn = navn;
        this.verdi = verdi;
    }

    public static String utregning(float antall, Valuta valuta) {
        float omgjoring = antall * valuta.verdi;
        String strOmgjoring = String.format("%.2f", omgjoring);
        String svar = antall + " " + valuta.navn + " blir til " + strOmgjoring + " nok.";
        return svar;
    }

}
