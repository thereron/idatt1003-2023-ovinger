package Ovinger.oving4.Oppgave2;

public class Main {

    public static Spiller[] spillere = new Spiller[2];


    public static void main(String[] args) {
        spillere[0] = new Spiller("Åse", 0);
        spillere[1] = new Spiller("Per", 0);

        startSpill();
    }

    public static void startSpill() {
        int runde = 0;
        while (!spillere[0].erFerdig() && !spillere[1].erFerdig()) {
            runde ++;
            int aktivSpiller = 0;
            while (aktivSpiller < 2) {
                int kast = spillere[aktivSpiller].kastTerningen();
                if (kast == 1) {
                    spillere[aktivSpiller].sumPoeng = 0;
                } else {
                    spillere[aktivSpiller].sumPoeng += kast;
                }
                aktivSpiller++;
            }
            System.out.println("--- Runde " + runde + " ---");
            System.out.println(spillere[0].navn + ": " + spillere[0].sumPoeng + " poeng");
            System.out.println(spillere[1].navn + ": " + spillere[1].sumPoeng + " poeng");
            System.out.println(" ");
        }
        if (spillere[0].sumPoeng >= 100 && spillere[1].sumPoeng < 100) {
            System.out.println(spillere[0].navn + " vant spillet!");
        } else if (spillere[0].sumPoeng < 100) {
            System.out.println(spillere[1].navn + " vant spillet!");
        } else {
            System.out.println("Begge spillere fikk 100 eller mer poeng på samme runde.");
        }
    }
}
