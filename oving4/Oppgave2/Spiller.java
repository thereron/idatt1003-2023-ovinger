package Ovinger.oving4.Oppgave2;

import java.util.Random;
public class Spiller {
    String navn;
    int sumPoeng;


    public Spiller(String navn, int sumPoeng) {
        this.navn = navn;
        this.sumPoeng = sumPoeng;
    }

    public int getSumPoeng() {
        return sumPoeng;
    }

    public int kastTerningen() {
        java.util.Random terning = new java.util.Random();
        int terningkast = terning.nextInt(6);
        return terningkast + 1;
    }

    public boolean erFerdig() {
        int sum = getSumPoeng();
        if (sum < 100) {
            return false;
        } else {
            return true;
        }
    }
}
