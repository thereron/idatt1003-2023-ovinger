package Ovinger.oving5;

import java.util.*;
public class MinRandom {
    Random tilfeldigTall = new Random();
    public int nesteHeltall(int nedre, int ovre) {
        if (nedre >= ovre) {
            throw new IllegalArgumentException("Den nedre grensen kan ikke være større enn eller lik den øvre grensen.");
        }
        int heltall = tilfeldigTall.nextInt(ovre - nedre + 1) + nedre;
        return heltall;
    }
    public double nesteDesimaltall(double nedre, double ovre) {
        if (nedre >= ovre) {
            throw new IllegalArgumentException("Den nedre grensen kan ikke være større enn eller lik den øvre grensen.");
        }
        double desimalTall = tilfeldigTall.nextDouble() * (ovre - nedre) + nedre;
        return desimalTall;
    }

    public static void main(String[] args) {
        MinRandom minRandom = new MinRandom();

        System.out.println("Heltall mellom 13 og 25:");
        for (int i = 0; i < 10; i++) {
            System.out.println(minRandom.nesteHeltall(13, 25));
        }
        System.out.println("Desimaltall mellom 2 og 19:");
        for (int i = 0; i < 10; i++) {
            System.out.println(minRandom.nesteDesimaltall(2, 19));
        }

    }
}
