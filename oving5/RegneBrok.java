package Ovinger.oving5;

public class RegneBrok {
    double teller;
    double nevner;
    public RegneBrok(double teller, double nevner){
        if (nevner == 0) {
            throw new IllegalArgumentException("Nevneren kan ikke være 0.");
        } else {
            this.teller = teller;
            this.nevner = nevner;
        }
    }
    public RegneBrok(double teller) {
        this(teller, 1);
    }

    public double getTeller() {
        return teller;
    }
    public double getNevner() {
        return nevner;
    }
    public void summere(RegneBrok annenBrok) {
        double nyNevner = this.nevner * annenBrok.getNevner();
        double nyTeller = (this.teller * annenBrok.getNevner()) + (annenBrok.getTeller() * this.nevner);
        this.nevner = nyNevner;
        this.teller = nyTeller;
    }
    public void subtrahere(RegneBrok annenBrok) {
        double nyNevner = this.nevner * annenBrok.getNevner();
        double nyTeller = (this.teller * annenBrok.getNevner()) - (annenBrok.getTeller() * this.nevner);
        this.nevner = nyNevner;
        this.teller = nyTeller;
    }
    public void multiplisere(RegneBrok annenBrok) {
        double nyNevner = this.nevner * annenBrok.getNevner();
        double nyTeller =  this.teller * annenBrok.getTeller();
        this.nevner = nyNevner;
        this.teller = nyTeller;
    }
    public void dele(RegneBrok annenBrok) {
        if (annenBrok.getTeller() == 0) {
            throw new IllegalArgumentException("Du kan ikke dele på en brøk som har 0 som teller.");
        } else {
            double nyTeller = this.teller * annenBrok.getNevner();
            double nyNevner = this.nevner * annenBrok.getTeller();
            this.nevner = nyNevner;
            this.teller = nyTeller;
        }
    }
    public String toString() {
        return teller + "/" + nevner;
    }

    public static void main(String[] args) {
        RegneBrok brok1 = new RegneBrok(1,2);
        RegneBrok brok2 = new RegneBrok(4,3);

        System.out.println("Brøk 1: " + brok1);
        System.out.println("Brøk 2: " + brok2);

        brok1.summere(brok2);
        System.out.println(brok1);

        brok1.subtrahere(brok2);
        System.out.println(brok1);

        brok1.multiplisere(brok2);
        System.out.println(brok1);

        brok1.dele(brok2);
        System.out.println(brok1);

    }
}
