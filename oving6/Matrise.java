package Ovinger.oving6;

import java.util.*;

public class Matrise {
    private final int[][] matrise;
    public Matrise(int[][] matrise) {
        this.matrise = new int[matrise.length][matrise[0].length];
        for (int i = 0; i < matrise.length; i++) {
            for (int j = 0; j < matrise[0].length; j++) {
                this.matrise[i][j] = matrise[i][j];
            }
        }
    }

    public Matrise adderMatrise(Matrise annenMatrise) {
        if (matrise.length != annenMatrise.matrise.length || matrise[0].length != annenMatrise.matrise[0].length) {
            return null;
        }

        int rader = matrise.length;
        int kolonner = matrise[0].length;
        int[][] resultatMatrise = new int[rader][kolonner];

        for (int i = 0; i < rader; i++) {
            for (int j = 0; j < kolonner; j++) {
                resultatMatrise[i][j] = matrise[i][j] + annenMatrise.matrise[i][j];
            }
        }

        return new Matrise(resultatMatrise);
    }

    public Matrise multipliserMatrise(Matrise annenMatrise) {
        if (matrise[0].length != annenMatrise.matrise.length) {
            return null;
        }

        int rader = matrise.length;
        int kolonner = annenMatrise.matrise[0].length;
        int[][] resultatMatrise = new int[rader][kolonner];

        for (int i = 0; i < rader; i++) {
            for (int j = 0; j < kolonner; j++) {
                int sum = 0;
                for (int k = 0; k < matrise[0].length; k++) {
                    sum += this.matrise[i][k] * annenMatrise.matrise[k][j];
                }
                resultatMatrise[i][j] = sum;
            }
        }

        return new Matrise(resultatMatrise);
    }

    public Matrise transponerMatrise() {
        int[][] resultatMatrise = new int[matrise[0].length][matrise.length];
        for (int i = 0; i < resultatMatrise.length; i++) {
            for (int j = 0; j < resultatMatrise[0].length; j++) {
                resultatMatrise[i][j] = matrise[j][i];
            }
        }
        return new Matrise(resultatMatrise);
    }


    /*@Override //husk på dette!! Best å bruke toString!!
    public String toString() {
        String result = "jfewhwfiu";

        return result;
    }*/

    public void skrivUt() {
        for (int i = 0; i < matrise.length; i++) {
            for (int j = 0; j < matrise[0].length; j++) {
                System.out.print(matrise[i][j] + " "); //gir mellomrom mellom tallene kolonnvis
            }
            System.out.println(); //gjør at vi får radene under hverandre
        }

    }
}
