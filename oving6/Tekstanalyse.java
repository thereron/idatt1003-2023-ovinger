package Ovinger.oving6;


public class Tekstanalyse {
    String StrAlfabetet = "abcdefghijklmnopqrstuvwxyzæøå";
    String tekst;
    int[] antallTegn;
    public Tekstanalyse(String tekst) {
        this.tekst = tekst;
        antallTegn = new int[30];

        tekst = tekst.toLowerCase();
        for (int i = 0; i < tekst.length(); i++) {
            char aktBokstav = tekst.charAt(i);
            int index = StrAlfabetet.indexOf(aktBokstav);
            if (index == -1) {
                antallTegn[29] += 1;
            } else {
                antallTegn[index] +=1;
            }
        }
    }

    public int getAntForsBokst () {
        int antforskjellig = 0;
        for (int i = 0; i < antallTegn.length - 1; i++) { //tar -1 fordi vi kun skal sjekke tot antall ulike bokstaver og ikke andre tegn
            if (antallTegn[i] > 0) {
                antforskjellig ++;
            }
        }
        return antforskjellig;
    }

    public int getTotAntBokst() {
        int totBokst = 0;
        for (int i = 0; i < antallTegn.length - 1; i++) {
            if (antallTegn[i] > 0) {
                totBokst += antallTegn[i];
            }
        }
        return totBokst;
    }

    public double getProsentIkkeBoskt() {
        int antBokstaver = getTotAntBokst();
        int antIkkeBokstaver = antallTegn[29];
        int tot = antBokstaver + antIkkeBokstaver;
        return 100 * antIkkeBokstaver/(tot+0.0);
    }

    public int getSpesBokstav(String bokstav) {
        int index = StrAlfabetet.indexOf(bokstav);
        return antallTegn[index];
    }

    public String getStorstForekomst() {
        StringBuilder storstForeBokst = new StringBuilder();
        int storst = 0;
        for (int i = 0; i < 29; i++) {
            char aktBokstav = StrAlfabetet.charAt(i);
            String strAktBokstav = Character.toString(aktBokstav);
            if (antallTegn[i] > storst) {
                storst = antallTegn[i];
                storstForeBokst = new StringBuilder(strAktBokstav);
            } else if (antallTegn[i] == storst) {
                storstForeBokst.append(", ").append(strAktBokstav);
            }
        };
        return storstForeBokst.toString();
    }
}


