package Ovinger.oving6;

import java.util.*;

public class henteTall {
    public static void main(String[] args) {
        Random random = new Random();

        int[] liste = new int[10];
        for(int i = 0; i < liste.length; i++) {
            liste[i] = i;
        }

        int gjennomlop = 10000;
        int[] telleListe = new int[10];

        for(int i = 0; i < gjennomlop; i++) {
            int tall = random.nextInt(10);
            for(int j = 0; j < liste.length; j++) {
                if(liste[j] == tall) {
                    telleListe[j]++;
                }
            }
        }

        printUt(liste, telleListe, gjennomlop);
    }
    public static void printUt(int[] listeMTall, int[] listeMTelling, int antGjennomlop) {
        for(int i = 0; i < listeMTall.length; i++) {
            String stjerner = "*";
            int antallRep = (int) Math.round(listeMTelling[i]/(antGjennomlop/100.0));
            String RepStjerner = stjerner.repeat(antallRep);
            System.out.println(listeMTall[i] + ": " + listeMTelling[i] + " " + RepStjerner);
        }
    }
}
