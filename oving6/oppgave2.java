package Ovinger.oving6;

import java.util.*;

public class oppgave2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Skriv teksten du ønsker å analysere (trykk enter uten å skrive noe hvis du ønsker å stoppe): ");
        String brukerInput = scan.nextLine();

        while (!brukerInput.isEmpty()) {
            Tekstanalyse tekstanalyse = new Tekstanalyse(brukerInput);
            System.out.println("Hvilken bokstav ønsker du å sjekke forekomst av?");
            String bokstavsjekk = scan.nextLine();
            skrivUt(tekstanalyse, bokstavsjekk.toLowerCase());

            System.out.println("Skriv teksten du ønsker å analysere (trykk enter uten å skrive noe hvis du ønsker å stoppe): ");
            brukerInput = scan.nextLine();
        }

        System.out.println("Du har avsluttet programmet.");
    }
    public static void skrivUt(Tekstanalyse teksten, String bokstav) {
        System.out.println("Antall forskjellige bokstaver i teksten: " + teksten.getAntForsBokst());
        System.out.println("Antall bokstaver totalt: " + teksten.getTotAntBokst());
        System.out.println("Andel prosent som ikke er bokstaver" + teksten.getProsentIkkeBoskt() + "%.");
        System.out.println("Forekomst av bokstaven " + bokstav + ": " + teksten.getSpesBokstav(bokstav));
        System.out.println("Bokstaven(e) som har størst forekomst: " + teksten.getStorstForekomst());
        System.out.println("---------------------------------");
    }
}
