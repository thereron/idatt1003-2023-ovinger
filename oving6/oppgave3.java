package Ovinger.oving6;

public class oppgave3 {
    public static void main(String[] args) {
        int[][] matrise1 = {{1, 6},{4, 5}};
        int[][] matrise2 = {{1, 2}, {7, 0}};

        Matrise m1 = new Matrise(matrise1);
        Matrise m2 = new Matrise(matrise2);

        Matrise adderRes = m1.adderMatrise(m2);
        Matrise multipliserRes = m1.multipliserMatrise(m2);
        Matrise transponerRes = m1.transponerMatrise();


        //Printing av resultater
        System.out.println("Matrise 1:");
        m1.skrivUt();

        System.out.println("Matrise 2:");
        m2.skrivUt();

        if (adderRes == null) {
            System.out.println("\nAddering av matrisene gikk ikke. \n");
        } else {
            System.out.println("Adderingen av matrisene ble slik:");
            adderRes.skrivUt();
        }

        if(multipliserRes == null) {
            System.out.println("Multiplisering gikk ikke. \n");
        } else {
            System.out.println("Multiplisereingen av matrisene ble slik:");
            multipliserRes.skrivUt();
        }
        System.out.println("Transponeringen av matrise 1 ble slik: ");
        transponerRes.skrivUt();

    }
}
