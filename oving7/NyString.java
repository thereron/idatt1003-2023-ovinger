package Ovinger.oving7;

import java.util.Scanner;

public class NyString {
    private final String string;

    public NyString(String string) {
        this.string = string;
    }

    public String forkort() {
        String[] ordListe = string.split(" ",0);

        StringBuilder resultat = new StringBuilder();
        for (String s : ordListe) {
            resultat.append(s.charAt(0));
        }
        return resultat.toString();
    }

    public String fjerneTegn(String bokstav) {
        String resultat = this.string;

        while(resultat.contains(bokstav)) { //så så fort vi får -1 (altså at det ikke er noe igjen av bokstaven), vil løkka stoppe
            int indeks = resultat.indexOf((bokstav));
            resultat = resultat.substring(0,indeks) + resultat.substring(indeks + 1); //ved å plusse på denne andre biten sørger vi for at vi får beholdt delen etter teksten vi har fjernet.
        }

        return resultat;
    }

    public static void main(String[] args) {
        kjorProgram();
    }

    public static void kjorProgram() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Skriv inn tekst: ");
        String inputTekst = scan.nextLine();
        NyString tekst = new NyString(inputTekst);

        fortsettProgram(tekst);
    }

    public static void fortsettProgram(NyString teksten) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Skriv 1 hvis du ønsker å forkorte teksten din.");
        System.out.println("Skriv 2 hvis du ønsker å fjerne en bokstav");
        int metodeValg = scan.nextInt();

        switch (metodeValg) {
            case 1: System.out.println(teksten.forkort());
                break;
            case 2: System.out.println("Skriv inn den bokstaven du ønsker å fjerne");
                scan.nextLine();
                String inputBokst = scan.nextLine();
                while (inputBokst.length() > 1) {
                    System.out.println("Du skal kun skrive en bokstav.");
                    inputBokst = scan.nextLine();
                }
                System.out.println(teksten.fjerneTegn(inputBokst));
                break;
            default: System.out.println("Du må skrive 1 eller 2");
                     fortsettProgram(teksten);
                     break;
        }
        kjorProgram();
    }
}

