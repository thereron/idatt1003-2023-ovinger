package Ovinger.oving7;

public class Tekstbehandling {
    private final String string;
    public Tekstbehandling(String string) {
        this.string = string;
    }

    public int antallOrd() {
        String[] ordListe = string.split(" ",0);
        return ordListe.length;
    }

    public double gjennomsnittOrdlengde() {
        String utenSkilletegn = string.replaceAll("[/!,?.:;()-]", "");
        String[] ordListe = utenSkilletegn.split(" ", 0);

        int totBokst = 0;
        for (String s : ordListe) {
            totBokst += s.length();
        }
        return (double) totBokst /ordListe.length;
    }

    public double gjennomsnittOrdPerPeriode() {
        String[] ordListe = string.split(" ", 0);
        String[] periodeListe = string.split("[!?.:]", 0);

        return (double) ordListe.length/periodeListe.length;
    }

    public String endreOrd(String gammeltOrd, String nyttOrd) {
        return this.string.replace(gammeltOrd, nyttOrd);
    }

    public String visString() {
        return this.string;
    }

    public String storeBokstaver() {
        String resultat = string;
        resultat = resultat.toUpperCase();
        return resultat;
    }
}
