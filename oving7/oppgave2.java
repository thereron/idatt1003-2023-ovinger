package Ovinger.oving7;

import java.util.Scanner;

public class oppgave2 {
    public static void main(String[] args) {
        kjorProgram();
    }

    public static void kjorProgram() {
        Scanner scan = new Scanner(System.in);

        System.out.println("Skriv inn teksten du ønsker å analysere:");
        String input = scan.nextLine();
        Tekstbehandling tekst = new Tekstbehandling(input);

        int valgTjeneste = 0;

        while(valgTjeneste != 7) {
            System.out.println("Tast 1 for å finne antall ord i teksten.");
            System.out.println("Tast 2 for å finne gjennomsnittlig ordlengde.");
            System.out.println("Tast 3 for å finne gjennomsnittlig antall ord per periode.");
            System.out.println("Tast 4 for å skifte ut et ord med et annet.");
            System.out.println("Tast 5 for å hente ut teksten din uten endringer.");
            System.out.println("Tast 6 for å hente ut teksten i store bokstaver.");
            System.out.println("Tast 7 for å avslutte og starte på nytt.");

            valgTjeneste = scan.nextInt();
            switch (valgTjeneste) {
                case 1:
                    System.out.println("Antall ord er: " + tekst.antallOrd());
                    break;
                case 2:
                    System.out.println("Gjennomsnittlig ordlengde er: " + tekst.gjennomsnittOrdlengde());
                    break;
                case 3:
                    System.out.println("Gjennomsnittlig antall ord per periode er: " + tekst.gjennomsnittOrdPerPeriode());
                    break;
                case 4:
                    System.out.println("Hvilket ord vil du skifte ut?");
                    String ord1 = scan.next();
                    System.out.println("Hvilket ord vil du bytte det til?");
                    String ord2 = scan.next();
                    System.out.println("Resultat: " + tekst.endreOrd(ord1, ord2));
                    break;
                case 5:
                    System.out.println("Din original tekst er: " + tekst.visString());
                    break;
                case 6:
                    System.out.println("Teksten i store bokstaver: " + tekst.storeBokstaver());
                    break;
                case 7:
                    System.out.println("Du vil nå starte på nytt av.");
                    break;
                default:
                    System.out.println("Husk å skrive et tall mellom 1 og 7.");
            }
        }
        kjorProgram();
    }
}
