package Ovinger.oving8;

import java.util.*;

public class ArbTaker {
    int aar = new GregorianCalendar().get(Calendar.YEAR);
    private final Person personalia;
    private final int arbtakernr, ansettelsesaar;
    private double maanedslonn, skatteprosent;

    public ArbTaker(Person personalia, int arbtakernr, int ansettelsesaar, double maanedslonn, double skatteprosent) {
        this.personalia = personalia;
        this.arbtakernr = arbtakernr;
        this.ansettelsesaar = ansettelsesaar;
        this.maanedslonn = maanedslonn;
        this.skatteprosent = skatteprosent; //25% skrives 25
    }
    public Person getPersonalia() {
        return personalia;
    }
    public int getArbtakernr() {
        return arbtakernr;
    }
    public int getAnsettelsesaar() {
        return ansettelsesaar;
    }
    public double getMaanedslonn() {
        return maanedslonn;
    }
    public double getSkatteprosent() {
        return skatteprosent;
    }
    public void setMaanedslonn(double nylonn) {
        this.maanedslonn = nylonn;
    }

    public void setSkatteprosent(double nySkatteprosent) {
        this.skatteprosent = nySkatteprosent;
    }

    public double skattPerMaaned() {
        return this.getMaanedslonn()*this.getSkatteprosent()/100;
    }
    public double bruttoLonnAar() {
        return Math.round(this.getMaanedslonn()*12.0);
    }
    public double skattPerAar() {
        return this.getMaanedslonn()*(10.5); //da får vi ti hele måneder (juni er skattefri), og en halv skatt for desember.
    }
    public String navn() {
        return this.getPersonalia().getEtternavn() + ", " + this.getPersonalia().getFornavn();
    }
    public int alder() {
        return aar - this.getPersonalia().getFodselaar();
    }
    public int antallAarAnsatt() {
        return aar - this.getAnsettelsesaar();
    }
    public boolean ansattMerEnn(int antAar) {
        return antallAarAnsatt() >= antAar;
    }

}
