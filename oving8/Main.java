package Ovinger.oving8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Person personKari = new Person("Kari", "Traa", 1974);
        ArbTaker arbTaker = new ArbTaker(personKari, 185731, 2022, 67198.18, 30); //legge disse to som objekter i main

        System.out.println("Arbeistakernummer: " + arbTaker.getArbtakernr());
        System.out.println("Navn: " + arbTaker.navn());
        System.out.println("Alder: " + arbTaker.alder());
        System.out.printf("Skattetrekk per måned: %.2f \n", arbTaker.skattPerMaaned());
        System.out.printf("Skattetrekk per år: %.2f \n", arbTaker.skattPerAar());
        System.out.printf("Bruttolønn per år: %.2f \n", arbTaker.bruttoLonnAar());
        System.out.println("Ansettelsesår: "  + arbTaker.getAnsettelsesaar() + ", og dermed vært ansatt i " + arbTaker.antallAarAnsatt() + " år.");
        System.out.print("Ansatt i mer enn 5 år? ");
        if (arbTaker.ansattMerEnn(5)) {
            System.out.println("Ja \n");
        } else {
            System.out.println("Nei \n");
        }
        vismeny(arbTaker);
    }
    public static void vismeny(ArbTaker arbtaker) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Tast 1 hvis du ønsker å endre skatteprosenten. ");
        System.out.println("Tast 2 hvis du ønsker å endre månedslønnen.");
        System.out.println("Tast 3 hvis du ønsker å avslutte.");
        int metodeInput = scan.nextInt();
        double brukerInput;

        switch(metodeInput){
            case 1:
                System.out.println("Oppgi den nye skatteprosenten du ønsker å endre til (skriv 25 for 25%): ");
                brukerInput = scan.nextDouble();
                while(brukerInput < 0 || brukerInput > 100) {
                    System.out.println("Du gå et ugyldig tall, prøv igjen: (Husk at du skal skrive f.eks. 25 for 25%.)");
                    brukerInput = scan.nextDouble();
                }
                arbtaker.setSkatteprosent(brukerInput);
                System.out.println("Den nye skatteprosenten er nå " + arbtaker.getSkatteprosent());
                vismeny(arbtaker);
                break;
            case 2:
                System.out.println("Oppgi den nye månedslønnen du ønsker å endre til: ");
                brukerInput = scan.nextDouble();
                while (brukerInput < 0) {
                    System.out.println("Du ga et ugyldig tall, prøv igjen:");
                    brukerInput = scan.nextDouble();
                }
                arbtaker.setMaanedslonn(brukerInput);
                System.out.println("Den nye månedslønnen er nå: " + arbtaker.getMaanedslonn());
                vismeny(arbtaker);
                break;
            case 3:
                System.out.println("Du har valgt å avslutte programmet.");
                break;
            default:
                System.out.println("Noe gikk galt. Husk at du skal taste 1, 2 eller 3.");
                vismeny(arbtaker);
                break;
        }


    }
}
