package Ovinger.oving8;

public class Person {
    private final String fornavn;
    private final String etternavn;
    private final int fodselaar;
    public Person(String fornavn, String etternavn, int fodselaar) {
        this.fornavn = fornavn;
        this.etternavn = etternavn;
        this.fodselaar = fodselaar;
    }

    public String getFornavn() {
        return fornavn;
    }
    public String getEtternavn() {
        return etternavn;
    }
    public int getFodselaar() {
        return fodselaar;
    }
}
