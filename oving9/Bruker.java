package Ovinger.oving9;

import java.util.Scanner;

public class Bruker {
  private static Scanner scan = new Scanner(System.in);
  private static Oppgaveoversikt oppgaveoversikt;

  public static void main(String[] args) {
    init();
    start();
  }

  public static void init() {
    oppgaveoversikt = new Oppgaveoversikt();
    oppgaveoversikt.registrerStudent("Trine", 3);
    oppgaveoversikt.registrerStudent("Ole", 0);
  }
  public static void start() {
    while (true) {
      printMeny();
    }
  }

  public static void printMeny() {

    System.out.println("MENY");
    System.out.println("Tast 1 for å finne antall studenter som er registrert.");
    System.out.println("Tast 2 for å finne antall oppgaver som en bestemt student har løst.");
    System.out.println("Tast 3 for å øke antall oppgaver for en bestemt student");
    System.out.println("Tast 4 for å registrere en ny student og antall oppgaver godkjent.");
    System.out.println("Tast 5 for å  se en oversikt over alle studentene.");
    int input = scan.nextInt();

    if(input ==1) {
      System.out.println("Antall studenter: " + oppgaveoversikt.getAntStud());
    } else if (input == 2) {
      System.out.println("Skriv navnet til studenten:");
      String navnInput = scan.next();
      if (!oppgaveoversikt.sjekkStudFinnes(navnInput)) {
        System.out.println("Du skrev inn et navn som ikke finnes i oppgaveoversikten.");
      } else {
        System.out.println(oppgaveoversikt.antOppgaver(
            oppgaveoversikt.getStudenter().get(oppgaveoversikt.finnStudent(navnInput)))); //dette her kan gjøres om til en getAntOppgByStudentName()-metode i oppgaveoversikt
        //i tillegg skal man gjerne ikke ha en metode for getStudenter, slik at man ikke henter ut hele denne i klienten for å finne en bestemt student. Gjerne skal jeg ikke ha en
        //getStudenter-metode i oppgaveoversikt, da dette skal være privat. (kan en slik metode i så fall gjøres privat?)
      }
    } else if (input == 3) {
      System.out.println("Skriv navnet til studenten:");
      String navnInput = scan.next();
      if (!oppgaveoversikt.sjekkStudFinnes(navnInput)) {
        System.out.println("Du skrev inn et navn som ikke finnes i oppgaveoversikten.");
      } else {
        System.out.println("Skriv antall oppgaer som skal legges til:");
        int oppgInput = scan.nextInt();
        Student aktuellStudent =
            oppgaveoversikt.getStudenter().get(oppgaveoversikt.finnStudent(navnInput));
        oppgaveoversikt.okAntOppgaver(aktuellStudent, oppgInput);
        System.out.println("La til " + oppgInput + " oppgaver.");
        System.out.println(aktuellStudent.toString());
      }
    } else if (input == 4) {
      System.out.println("Skriv navnet til studenten du ønsker å registrere:");
      String navnInput = scan.next();
      if (oppgaveoversikt.sjekkStudFinnes(navnInput)) {
        System.out.println("Du skrev inn et navn som allerede finnes i oppgaveoversikten.");
      } else {
        System.out.println("Skriv antall oppgaver studenten ha gjort til nå:");
        int oppgInput = scan.nextInt();
        oppgaveoversikt.registrerStudent(navnInput, oppgInput);
      }
    } else if (input == 5) {
      System.out.println(oppgaveoversikt.toString());
    } else {
      System.out.println("Noe gikk galt, prøv igjen.");
    }
  }
}
