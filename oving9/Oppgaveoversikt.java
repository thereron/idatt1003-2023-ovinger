package Ovinger.oving9;

import java.util.ArrayList;
import java.util.Objects;

public class Oppgaveoversikt {
  private ArrayList<Student> studenter;
  private int antStud = 0;

  public Oppgaveoversikt() {
    this.studenter = new ArrayList<Student>();
  }

  public ArrayList<Student> getStudenter() {
    return studenter;
  }

  public int getAntStud() {
    return antStud;
  }
  public int antOppgaver(Student student) {
    return student.getAntOppg();
  }
  public void okAntOppgaver(Student student, int okning) {
    student.okAntOppg(okning);
  }
  public void registrerStudent(String navn, int antOppg) {
    Student student = new Student(navn, antOppg);
    studenter.add(student);
    antStud = studenter.size();
  }

  public boolean sjekkStudFinnes(String navn) {
    if (finnStudent(navn) == -1) {
      return false;
    } else {
      return true;
    }
  }
  public int finnStudent(String navn) {
    int indeks = -1;
    for(int i = 0; i < getAntStud(); i++) {
      if (Objects.equals(getStudenter().get(i).getNavn(), navn)) {
        indeks = i;
      }
    }
    return indeks;
  }


  @Override
  public String toString() {
    StringBuilder tekst = new StringBuilder();
    for(int i = 0; i < getAntStud(); i++) {
      tekst.append(getStudenter().get(i).toString()).append("\n");
    }
    return tekst.toString();
  }
}
