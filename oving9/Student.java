package Ovinger.oving9;

public class Student {
  private final String navn;
  private int antOppg;

  public Student(String navn, int antOppg) {
    this.navn = navn;
    this.antOppg = antOppg;
  }

  public String getNavn() {
    return navn;
  }

  public int getAntOppg() {
    return antOppg;
  }

  public void okAntOppg(int okning) {
    this.antOppg = this.antOppg + okning;
  }

  @Override
  public String toString() {
    return getNavn() + ": " + getAntOppg() + " oppgaver godkjent.";
  }

  //enkelt testprogram
  public static void main(String[] args) {
    Student student = new Student("Kåre", 4);
    System.out.println(student.toString());
    System.out.println("Legger til 6 oppgaver.");
    student.okAntOppg(6);
    System.out.println(student.toString());
  }
}
